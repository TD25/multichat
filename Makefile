CC=gcc
CFLAGS=-I./src/ -Wall -g
CURSES_LIBDIR=./ncurses-6.0
#LIBS=$(CURSES_LIB)/libform.a $(CURSES_LIB)/libmenu.a $(CURSES_LIB)/libpanel.a $(CURSES_LIB)/libncurses.a 
LIBS=$(CURSES_LIBDIR)/libncurses.a 

all: multichatc multichatd

multichatc: src/multichatc.c src/chatc_ui.c
	$(CC) -o multichatc src/multichatc.c src/chatc_ui.c $(LIBS) $(CFLAGS) -L $(CURSES_LIBDIR)

multichatd: src/multichatd.c
	$(CC) -o multichatd src/multichatd.c $(CFLAGS)

clean:
	rm multichatc multichatd

