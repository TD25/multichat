#include "chatc_ui.h"
#include <string.h>
#include <signal.h>


void cui_init(struct cui_data* data) {
	// initialize stdwin
	data->stdwin = initscr();
	wresize(data->stdwin, STDWIN_LINES, STDWIN_COLS);
	move(STDWIN_LINES-1, 0);
	scrollok(data->stdwin, TRUE);

	data->separator = newwin(1, STDWIN_COLS, STDWIN_LINES, 0); 
	whline(data->separator, '-', STDWIN_COLS);
	wrefresh(data->separator);

	data->inputwin = newwin(INPUTWIN_LINES, INPUTWIN_COLS, STDWIN_LINES+1, 0);
	scrollok(data->inputwin, TRUE);
	refresh();
	wrefresh(data->inputwin);

	//signal(SIGWINCH, cui_resize_handler);
}

void cui_quit(struct cui_data* data) {
	delwin(data->inputwin);
	delwin(data->separator);
	endwin();

}

void cui_print_msg(char* buff, int len) {
	buff[len] = '\0';
	//mvprintw(STDWIN_LINES-1, 0, "\n%s", buff);
	printw("\n%s", buff);
}

// Returns number of characters read
int cui_get_input(struct cui_data* curs_data, char* buff, int buffsize) {
	if (wgetnstr(curs_data->inputwin, buff, sizeof buff) == ERR) {
		printw("error reading input");
		return -1;
	}
	wclear(curs_data->inputwin);
	return strlen(buff);
}

//void cui_resize_handler(int sig) {
//	endwin();
//	refresh();
//}

