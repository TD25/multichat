#include <ncurses.h>

#define INPUTWIN_COLS COLS
#define INPUTWIN_LINES 5
#define STDWIN_COLS COLS
#define STDWIN_LINES LINES - INPUTWIN_LINES

struct cui_data {
	WINDOW* stdwin;
	WINDOW* inputwin;
	WINDOW* separator;
	int stdwin_lines, stdwin_cols, inputwin_lines, inputwin_cols;
};

void cui_init(struct cui_data* data);
void cui_quit(struct cui_data* data);
void cui_print_msg(char* buff, int len);
int cui_get_input(struct cui_data* curs_data, char* buff, int buffsize);
//void cui_resize_handler(struct cui_data* curs_data);

