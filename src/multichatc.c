#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <ncurses.h>
#include "chatc_ui.h"

#define PORT "21111"
#define BUFF_SIZE 1024

// get sockaddr, IPv4 or IPv6:
void *get_in_addr(struct sockaddr *sa)
{
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

int connect_to_server(const char* addr, const char* port) {
    struct addrinfo hints, *ai, *p;
    // get us a socket and bind it
    memset(&hints, 0, sizeof hints);
    hints.ai_family = AF_UNSPEC;	//allow IPv6 as well as IPv4
    hints.ai_socktype = SOCK_STREAM;
	int rv, s_socket;
    if ((rv = getaddrinfo(addr, port, &hints, &ai)) != 0) {
        fprintf(stderr, "multichatc: %s\n", gai_strerror(rv));
        exit(1);
    }
    
    for(p = ai; p != NULL; p = p->ai_next) {
        s_socket = socket(p->ai_family, p->ai_socktype, p->ai_protocol);
        if (s_socket < 0) { 
            continue;
        }
        
        // lose the pesky "address already in use" error message
        //setsockopt(listener, SOL_SOCKET, SO_REUSEADDR, &yes, sizeof(int));

		if (connect(s_socket, p->ai_addr, p->ai_addrlen) < 0) {
			close(s_socket);
			continue;
		}

        break;
    }

    if (p == NULL) {
		// if we got here, it means we didn't get bound
        fprintf(stderr, "multichatc: failed to connect\n");
        exit(1);
    }

    freeaddrinfo(ai); // all done with this

	return s_socket;
}


int run(int s_socket, struct cui_data* curs_data) {
	fd_set read_fds, temp_fds;
	FD_ZERO(&read_fds);
	FD_SET(0, &read_fds);	//listen to stdin
	FD_SET(s_socket, &read_fds);
	int fdmax = s_socket;
	char buff[BUFF_SIZE];
	int bytes = 0;

	while (1) {
		temp_fds = read_fds;
        if (select(fdmax+1, &temp_fds, NULL, NULL, NULL) == -1) {
            perror("select");
            return 4;
        }
		if (FD_ISSET(s_socket, &temp_fds)) {
			//memset(buff, 0, BUFF_SIZE);
			if ((bytes = recv(s_socket, buff, sizeof buff, 0)) <= 0) {
				if (bytes == 0) {
					printf("multichatc: disconected from server\n");
				}
				else {
					perror("recv");
				}
				return 1;
			} else {
				cui_print_msg(buff, bytes);
			}
		}
		if (FD_ISSET(0, &temp_fds)) {
			if ((bytes = cui_get_input(curs_data, buff, sizeof buff)) < 0) {
				printw("error reading input");
				return 1;
			}
			send(s_socket, buff, bytes, 0);
			//add sent message to chat locally
			cui_print_msg(buff, bytes);
			
		}
		refresh();
		wrefresh(curs_data->inputwin);
	}
}

int main(int argc, char** args) {
	if (argc < 2) {
		printf("USAGE: multichatc <address>\n");
		exit(1);
	}
	int s_socket = connect_to_server(args[1], PORT);
	
	struct cui_data curs_data;
	cui_init(&curs_data);
	int r = run(s_socket, &curs_data);
	//printw("hello");
	//char buff[256];
	//wgetnstr(inputwin, buff, 256);
	//printw(buff);
	//getch();

	close(s_socket);
	// free ncurses resources
	//int r = 0;
	return r;
}

